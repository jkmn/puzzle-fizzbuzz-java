puzzle-fizzbuzz-java
====================

A standard Fizz/Buzz solution written in Java.

This code includes a few niceties to make life easier to bear:
* Columnized output
* Runtime specification of domain via command-line argument.

To specify the range, simply apply an integer as a command-line argument to indicate the desired range.
