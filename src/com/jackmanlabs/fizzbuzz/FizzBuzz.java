package com.jackmanlabs.fizzbuzz;

public class FizzBuzz {

	public static void main(String[] args) {

		// Default value for FizzBuzz limit.
		int limit = 100;

		// If specified, use the supplied limit/range.
		if (args.length == 1) {
			try {
				limit = Integer.parseInt(args[0]);
			} catch (Exception e) {
				System.out.println(e.getLocalizedMessage());
				System.exit(1);
			}
		}

		System.out.format(
				"Generating Fizz/Buzz output for numbers from 0 to %d.\n",
				limit);

		boolean fizz = false;
		boolean buzz = false;
		for (int i = 0; i <= limit; i++) {
			fizz = i % 3 == 0;
			buzz = i % 5 == 0;

			// All of these conditionals is to simply make things pretty.
			if (fizz || buzz) {
				System.out.format("%d:", i);

				if (fizz && buzz) {
					System.out.print("\tFizz!\tBuzz!");
				} else if (fizz) {
					System.out.print("\tFizz!");
				} else if (buzz) {
					System.out.print("\t\tBuzz!");
				}
				
				System.out.println();
			}

		}
	}

}
